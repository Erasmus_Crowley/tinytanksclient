﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTarget : MonoBehaviour {

    public Transform Target;

    public Vector3 PositionOffset;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		if (Target != null)
        {
            this.transform.position = Target.position + PositionOffset;
            this.transform.LookAt(Target);
        }
	}
}

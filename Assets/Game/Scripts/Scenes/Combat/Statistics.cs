﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statistics {
    public delegate void StatisticChangedEvent(int value);
    public static event StatisticChangedEvent OnTankCountChanged;

    private static int _tankCount;
    public static int TankCount
    {
        get
        {
            return _tankCount;
        }
        set
        {
            if (_tankCount != value)
            {
                _tankCount = value;

                if (OnTankCountChanged != null)
                    OnTankCountChanged(value);
            }
        }
    }
}

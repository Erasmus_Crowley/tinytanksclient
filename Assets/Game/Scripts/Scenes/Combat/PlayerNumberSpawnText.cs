﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerNumberSpawnText : MonoBehaviour {
    TMP_Text _textbox;


    private void Awake()
    {
        _textbox = GetComponent<TMP_Text>();
        _textbox.text = "Tanks spawned: 0";

        Statistics.OnTankCountChanged += OnTankCountChanged;
    }

    private void OnTankCountChanged(int value)
    {
        _textbox.text = "Tanks spawned: " + value;
    }
}

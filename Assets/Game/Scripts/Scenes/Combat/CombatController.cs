﻿using Metatron;
using Metatron.Connections;
using System.Collections;
using System.Collections.Generic;
using TinyTanksProtocol;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CombatController : MonoBehaviour {
    public Camera SpawnCamera;
    public Camera TankCamera;

    public LayerMask SpawnableAreaLayers;
    public GameObject TankPrefab;

    public int NetworkUpdatesPerSecond = 10;
    private float netUpdateTimer;
    private float netUpdateDelay;

    private Vector3 lastUpdatePos;
    private Vector3 lastUpdateAim;

    public TankController LocalTank;
    public Dictionary<string, TankController> RemoteTanks = new Dictionary<string, TankController>();

    public float MaxWeaponRange = 4f;

    private CombatState _state = CombatState.Spawning;
    private CombatState State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;

            if (_state == CombatState.Combat)
            {
                TankCamera.enabled = true;
                SpawnCamera.enabled = false;
            }

            if (_state == CombatState.Spawning)
            {
                TankCamera.enabled = false;
                SpawnCamera.enabled = true;
            }
        }
    }

    private RaycastHit mouseHitInfo;

    // Use this for initialization
    void Start () {
        this.State = CombatState.Spawning;

        this.netUpdateDelay = 1f / NetworkUpdatesPerSecond;

        Network.Client.OnDisconnected += OnDisconnection;

        Network.Client.AddMessageHandler(typeof(MessageTankSpawn), OnTankSpawned);
        Network.Client.AddMessageHandler(typeof(MessageTankKill), OnTankKilled);
        Network.Client.AddMessageHandler(typeof(MessageTankMove), OnTankMove);
        Network.Client.AddMessageHandler(typeof(MessageTankAim), OnTankAim);
        Network.Client.AddMessageHandler(typeof(MessageTankFire), OnTankFire);
        Network.Client.AddMessageHandler(typeof(MessageTankDamage), OnTankDamaged);

        Network.Client.Send(new MessageTankListRequest());
	}

    private void OnDestroy()
    {
        Network.Client.OnDisconnected -= OnDisconnection;

        Network.Client.RemoveMessageHandler(typeof(MessageTankSpawn), OnTankSpawned);
        Network.Client.RemoveMessageHandler(typeof(MessageTankKill), OnTankKilled);
        Network.Client.RemoveMessageHandler(typeof(MessageTankMove), OnTankMove);
        Network.Client.RemoveMessageHandler(typeof(MessageTankAim), OnTankAim);
        Network.Client.RemoveMessageHandler(typeof(MessageTankFire), OnTankFire);
        Network.Client.RemoveMessageHandler(typeof(MessageTankDamage), OnTankDamaged);
    }

    // Update is called once per frame
    void Update () {
        if (Application.isFocused)
        {
            switch(State)
            {
                case CombatState.Spawning:
                    UpdateSpawningState();
                    break;

                case CombatState.Combat:
                    UpdateCombatState();
                    break;
            }
        }
	}

    private void UpdateSpawningState()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SpawnLocalTank();
        }
    }

    private void UpdateCombatState()
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out mouseHitInfo))
        {
            if (Input.GetMouseButtonDown(0))
            {
                LocalTank.Move(mouseHitInfo.point);
                Network.Client.Send(new MessageTankMoveRequest(mouseHitInfo.point.x, mouseHitInfo.point.y, mouseHitInfo.point.z));
            }
            else
            {
                LocalTank.Aim(mouseHitInfo.point);
            }

            if (Input.GetMouseButtonDown(1))
            {
                //FIRE!
                Vector3 hit = LocalTank.CheckForLineOfSight(mouseHitInfo.point);
                Network.Client.Send(new MessageTankFireRequest(hit.x, hit.y, hit.z));
                LocalTank.Fire(hit);
            }
        }

        //Update the server with aim and pos
        netUpdateTimer += Time.deltaTime;

        if (netUpdateTimer >= netUpdateDelay)
        {
            netUpdateTimer = 0f;

            //Update pos
            Vector3 pos = LocalTank.transform.position;
            if (pos != lastUpdatePos)
            {
                Network.Client.Send(new MessageTankPositionUpdate(pos.x, pos.y, pos.z, LocalTank.transform.rotation.eulerAngles.y));
                lastUpdatePos = pos;
            }

            //Update aim
            Vector3 aim = LocalTank.AimTarget;
            if (aim != lastUpdateAim)
            {
                Network.Client.Send(new MessageTankAimRequest(aim.x, aim.y, aim.z));
                lastUpdateAim = aim;
            }
        }
    }




    #region Client Events

    private void OnDisconnection(NetClient client)
    {
        SceneManager.LoadScene(Scenes.Start);
    }

    #endregion

    #region Message Handlers

    private void OnTankSpawned(Connection sender, Message message)
    {
        MessageTankSpawn msg = (MessageTankSpawn)message;

        //Don't spawn a remote controlled tank for the current user.
        if (msg != null && msg.Username != Network.Username)
        {
            SpawnRemoteTank(msg.Username, new Vector3(msg.xPos, msg.yPos, msg.zPos), new Vector3(msg.xDest, msg.yDest, msg.zDest), new Vector3(msg.xAim, msg.yAim, msg.zAim), msg.orientation);
        }
    }

    private void OnTankKilled(Connection sender, Message message)
    {
        MessageTankKill msg = (MessageTankKill)message;

        //Don't spawn a remote controlled tank for the current user.
        if (msg != null)
        {
            if (msg.Username.Equals(Network.Username))
            {
                if (LocalTank != null)
                {
                    LocalTank.Die();
                    LocalTank = null;
                }

                //Go back to spawn mode!
                State = CombatState.Spawning;
            }
            else
            {
                TankController t = RemoteTanks[msg.Username];

                if (t != null)
                    t.Die();

                RemoteTanks.Remove(msg.Username);

                Statistics.TankCount -= 1;
            }
        }
    }

    private void OnTankAim(Connection sender, Message message)
    {
        MessageTankAim msg = (MessageTankAim)message;
        
        if (msg != null && !msg.Username.Equals(Network.Username))
        {
            TankController t = RemoteTanks[msg.Username];

            t.Aim(new Vector3(msg.X, msg.Y, msg.Z));
        }
    }

    private void OnTankMove(Connection sender, Message message)
    {
        MessageTankMove msg = (MessageTankMove)message;

        if (msg != null && !msg.Username.Equals(Network.Username))
        {
            TankController t = RemoteTanks[msg.Username];

            t.Move(new Vector3(msg.X, msg.Y, msg.Z));
        }
    }

    private void OnTankFire(Connection sender, Message message)
    {
        MessageTankFire msg = (MessageTankFire)message;

        if (msg != null)
        {
            if (RemoteTanks.ContainsKey(msg.Username))
            {
                Vector3 hit = new Vector3(msg.X, msg.Y, msg.Z);

                RemoteTanks[msg.Username].Fire(hit);

                //Check for proximity to explosion and check for obstacles.
                if (Vector3.Distance(LocalTank.transform.position, hit) < MaxWeaponRange)
                {
                    if (!Physics.Raycast(hit, LocalTank.transform.position, MaxWeaponRange))
                    {
                        Network.Client.Send(new MessageTankDamageRequest(Vector3.Distance(LocalTank.transform.position, hit)));
                    }
                }
            }
        }
    }

    private void OnTankDamaged(Connection sender, Message message)
    {
        MessageTankDamage msg = (MessageTankDamage)message;

        if (msg != null)
        {
            if (msg.Username.Equals(Network.Username))
            {
                LocalTank.Health = msg.Health;
            }
            else
            {
                if (RemoteTanks.ContainsKey(msg.Username))
                {
                    RemoteTanks[msg.Username].Health = msg.Health;
                }
            }
        }
    }

    #endregion

    #region Tank Spawning

    private void SpawnLocalTank()
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out mouseHitInfo, SpawnableAreaLayers))
        {
            LocalTank = GameObject.Instantiate(TankPrefab, mouseHitInfo.point, Quaternion.identity, null).GetComponent<TankController>();
            LocalTank.Name = Network.Username;
            LocalTank.Move(mouseHitInfo.point);

            State = CombatState.Combat;
            TankCamera.gameObject.GetComponent<CameraFollowTarget>().Target = LocalTank.transform;

            Network.Client.Send(new MessageTankSpawnRequest(mouseHitInfo.point.x, mouseHitInfo.point.y, mouseHitInfo.point.z));

            Statistics.TankCount += 1;
        }
    }

    private void SpawnRemoteTank(string username, Vector3 position, Vector3 destination, Vector3 aim, float orientation)
    {
        TankController tank = GameObject.Instantiate(TankPrefab, position, Quaternion.Euler(0f, orientation, 0f), null).GetComponent<TankController>();
        tank.Name = username;
        tank.Aim(aim);
        tank.Move(destination);

        RemoteTanks.Add(username, tank);

        Statistics.TankCount += 1;
    }

    #endregion
}

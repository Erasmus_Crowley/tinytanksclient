﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using Metatron;
using TinyTanksProtocol;
using UnityEngine.SceneManagement;

public class LoginButton : MonoBehaviour, IPointerClickHandler {

    public TMP_InputField ServerInput;
    public TMP_InputField UsernameInput;
    public TMP_Text WarningText;

    private void Start()
    {
        Network.Client.OnConnectSucceeded += OnConnectionSucceeded;
        Network.Client.OnConnectFailed += OnConnectionFailed;

        Network.Client.AddMessageHandler(typeof(MessageUsernameResponse), OnUsernameResponse);

        if (ServerInput != null)
            ServerInput.text = Network.Address;

        if (WarningText != null)
            WarningText.text = string.Empty;
    }

    private void OnDestroy()
    {
        Network.Client.OnConnectSucceeded -= OnConnectionSucceeded;
        Network.Client.OnConnectFailed -= OnConnectionFailed;

        Network.Client.RemoveMessageHandler(typeof(MessageUsernameResponse), OnUsernameResponse);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (UsernameInput == null || WarningText == null)
            return;

        if (string.IsNullOrWhiteSpace(UsernameInput.text))
        {
            WarningText.text = "Please enter a user name";
            return;
        }

        if (ServerInput != null && string.IsNullOrWhiteSpace(ServerInput.text))
        {
            ServerInput.text = Network.DefaultAddress;
            WarningText.text = "Network address not specified";
            return;
        }

        if (Network.Client.IsConnected)
        {
            //Debug.Log("Connected already. Requesting username!");
            SendUsernameRequest(UsernameInput.text.Trim());
        }
        else
        {
            //Debug.Log("Starting connection!");
            Network.Address = ServerInput.text;
            Network.Client.Connect(Network.Address, Network.Port);
        }
    }

    private void OnConnectionSucceeded(NetClient client)
    {
        if (!string.IsNullOrEmpty(UsernameInput.text))
        {
            SendUsernameRequest(UsernameInput.text);
        }
    }

    private void OnConnectionFailed(NetClient client, string reason)
    {
        WarningText.text = reason;
    }

    private void SendUsernameRequest(string username)
    {
        if (Network.Client.IsConnected)
        {
            Network.Client.Send(new MessageUsernameRequest(UsernameInput.text));
        }
    }

    private void OnUsernameResponse(Metatron.Connections.Connection sender, Message message)
    {
        MessageUsernameResponse response = (MessageUsernameResponse)message;

        if (response != null)
        {
            if (response.Accepted)
            {
                WarningText.text = "Username request accepted.";
                Network.Username = UsernameInput.text;
                SceneManager.LoadScene(Scenes.Combat);
            }
            else
            {
                WarningText.text = "Username is already in use. Please choose a different username.";
            }
        }
    }
}

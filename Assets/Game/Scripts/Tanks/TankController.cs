﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(NavMeshAgent))]
public class TankController : MonoBehaviour {
    public Transform TurretObject;

    [Header("GUI Configuration")]
    public TMP_Text NameText;
    public Scrollbar HealthBar;

    [Header("Weapon Configuration")]
    public Transform BulletSpawnLocation;
    public GameObject ExplosionPrefab;
    public GameObject TurretFirePrefab;
    public float TurretSpeed = 4f;

    [Header("Network Configuration")]
    private NavMeshAgent nav;
    public int NavUpdatesPerSecond = 4;
    private float navUpdateTimer;
    private float navUpdateDelay;
    private bool navUpdateQueued;

    private Vector3 destination;

    private RaycastHit mouseHitInfo;
    private RaycastHit fireHitInfo;

    private Vector3 _aimTarget;
    public Vector3 AimTarget
    {
        get
        {
            return _aimTarget;
        }
    }

    private float _health = 1.0f;
    public float Health
    {
        get { return _health; }
        set
        {
            _health = Mathf.Clamp(value, 0f, 1f);

            HealthBar.size = _health;
        }
    }

    public string Name
    {
        set
        {
            if (NameText != null)
                NameText.text = value;
        }
    }

    private void Start()
    {
        this.nav = GetComponent<NavMeshAgent>();

        this.navUpdateDelay = 1f / this.NavUpdatesPerSecond;
    }

    private void Update()
    {
        if (navUpdateQueued)
        {
            navUpdateTimer += Time.deltaTime;

            RecalculateNav();
        }

        if (TurretObject != null)
            TurretObject.rotation = Quaternion.Slerp(TurretObject.rotation, Quaternion.LookRotation(_aimTarget - TurretObject.position), Time.deltaTime * TurretSpeed);
    }

    public Vector3 CheckForLineOfSight(Vector3 target)
    {
        Vector3 hit = gameObject.transform.position;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out mouseHitInfo))
        {
            if (Physics.Raycast(BulletSpawnLocation.position, BulletSpawnLocation.forward, out fireHitInfo, Vector3.Distance(BulletSpawnLocation.position, mouseHitInfo.point)))
            {
                //If there is an object between the turret and the clicked object, return the point of collision.
                hit = fireHitInfo.point;
            }
            else
            {
                //Otherwise return the point where the user clicked.
                hit = mouseHitInfo.point;
            }
        }

        return hit;
    }

    public void Fire(Vector3 target)
    {
        if (TurretFirePrefab != null)
        {
            Instantiate(TurretFirePrefab, BulletSpawnLocation.position, Quaternion.identity, null);
        }


        if (ExplosionPrefab != null)
        {
            Instantiate(ExplosionPrefab, target, Quaternion.identity, null);
        }
    }

    public void Aim(Vector3 target)
    {
        this._aimTarget = target;
    }

    public void Move(Vector3 destination)
    {
        //Debug.Log("Moving to: " + destination);

        this.destination = destination;
        this.navUpdateQueued = true;

        if (nav != null)
            RecalculateNav();
    }

    private void RecalculateNav()
    {
        if (navUpdateTimer >= navUpdateDelay)
        {
            this.nav.SetDestination(destination);
            this.navUpdateQueued = false;
            this.navUpdateTimer = 0f;
        }
    }

    public void Die()
    {
        Destroy(this.gameObject);
    }
}

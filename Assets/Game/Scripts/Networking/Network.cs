﻿using Metatron;
using Metatron.Connections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Network {
    public static string Username = string.Empty;

    public static string DefaultAddress = "173.23.54.80";
    public static string Address;
    public static int Port = 20000;

    private static NetClient _client;
    public static NetClient Client
    {
        get
        {
            return _client;
        }
    }

    public static void Awake()
    {
        Address = DefaultAddress;
        _client = new NetClient(ThreadingMode.Synchronous);
        //Debug.Log("Initialized network client");
    }

    public static void Start()
    {
        
    }
	
    public static void Update()
    {
        _client.UpdateSynchronousEvents();
    }
}

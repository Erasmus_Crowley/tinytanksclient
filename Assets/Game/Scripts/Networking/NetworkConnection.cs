﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkConnection : MonoBehaviour {
    private void Awake()
    {
        DontDestroyOnLoad(this);
        Network.Awake();
    }

    // Use this for initialization
    void Start () {
        Network.Start();
	}
	
	// Update is called once per frame
	void Update () {
        Network.Update();
	}
}

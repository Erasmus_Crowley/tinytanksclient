﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour {
    public float Delay = 1000f;

    private float _timer = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        _timer += Time.deltaTime * 1000f;

        if (_timer > this.Delay)
            Destroy(this.gameObject);
	}
}

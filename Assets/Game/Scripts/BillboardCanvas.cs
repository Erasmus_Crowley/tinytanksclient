﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardCanvas : MonoBehaviour {
    public Vector3 Rotation;

    //private Vector3 startRotation;

	// Use this for initialization
	void Start () {
        //startRotation = transform.rotation.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
        //Vector3 camPos = Camera.main.transform.position;

        //transform.LookAt(camPos);

        this.transform.rotation = Quaternion.Euler(Rotation);
    }
}
